bayes-dota
==========

This is the [task](TASK.md).

**Payload Job**

On ingest payload a Match entity is persisted to generate a new match Id, an executor task is crated with newly created match id and payload
API call return the match id for future uses.

**Chunk Job**

Task executor split payload in a chunk of 100 lines, and handed over to another task executor.

**Line Parser**

Each line is parsed through Parser Chain one by one, if any of the parser is able to map the String line into MatchEvent successfully the chain is broken and transformed object is persisted in database as an event 

and Next line is picked.

**Chunk update**
 Each chunk on successful parsing notify the caller about completion. Once all chunks are processed the status of Match is updated from PENDING to PROCESSED
 
 
**Improvements**
    
1. Calculated stats can be saved in database after all chunks are processed
2. Integration tests can be added for more quality and API testing
3. Delay can be replaced with await functionality

**Sonar Qube**

![Sonar Report](assets/sonar-report.png)

**Test Coverage**

Test coverage for "java" directory

* classes: 97%
* lines covered: 91%

![Code coverage](assets/code-coverage.png)
