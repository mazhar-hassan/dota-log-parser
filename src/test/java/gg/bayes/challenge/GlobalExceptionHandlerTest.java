package gg.bayes.challenge;

import gg.bayes.challenge.common.exception.*;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.servlet.NoHandlerFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GlobalExceptionHandlerTest {

    @Test
    public void testRecordNotFoundException() {
        RecordNotFoundException exception = new RecordNotFoundException("Junit record not found");

        ResponseEntity<ExceptionResponse> response = new GlobalExceptionHandler().handleAllExceptions(exception);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testValidationException() {
        RequiredFieldValidationException exception = new RequiredFieldValidationException("Required Junit field is missing");

        ResponseEntity<ExceptionResponse> response = new GlobalExceptionHandler().handleAllExceptions(exception);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testException() {
        Exception exception = new Exception("Some unhandled exception occurred");

        ResponseEntity<ExceptionResponse> response = new GlobalExceptionHandler().handleAllExceptions(exception);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }


    @Test
    public void test404() {
        NoHandlerFoundException exception = new NoHandlerFoundException(HttpMethod.GET.toString(), "/v1/api", new HttpHeaders());
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod(HttpMethod.GET.toString());
        request.setRequestURI("/v1/api");

        ResponseEntity<ExceptionResponse> response = new GlobalExceptionHandler().handleError404(exception, request);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(ApplicationException.ERROR_ENDPOINT_NOT_FOUND, response.getBody().getCode());

    }

    @Test
    public void test404MethodNotSupported() {
        HttpRequestMethodNotSupportedException exception = new HttpRequestMethodNotSupportedException(HttpMethod.GET.toString(), null, "Get Method is not supported");
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setMethod(HttpMethod.GET.toString());
        request.setRequestURI("/v1/api");

        ResponseEntity<ExceptionResponse> response = new GlobalExceptionHandler().handleError404(exception, request);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(ApplicationException.ERROR_HTTP_METHOD_NOT_SUPPORTED, response.getBody().getCode());

    }


}
