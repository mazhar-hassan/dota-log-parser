package gg.bayes.challenge.service;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.repository.MatchEventRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class LogChunkProcessorTest {

    private static final String ACTOR = "junit_hero";
    private static long MATCH_ID = 901L;
    @Autowired
    ChunkProcessor chunkProcessor;

    @Autowired
    MatchEventRepository matchEventRepository;

    @Test
    public void testProcess() {
        List<String> chunk = new ArrayList<>();
        chunk.add("[00:12:47.067] npc_dota_hero_junit_hero buys item item_junit1_item");
        chunk.add("[00:12:47.067] npc_dota_hero_junit_hero buys item item_junit2_item");
        chunkProcessor.syncProcess(MATCH_ID, chunk);

        List<MatchEvent> events = matchEventRepository.getAllMatchEventByMatchIdAndActor(MATCH_ID, ACTOR);
        List<MatchEvent> boughtEvents = events.stream()
                .filter(event -> ACTOR.equals(event.getActor()))
                .filter(event -> "buy".equals(event.getAction()))
                .collect(Collectors.toList());

        assertNotNull(boughtEvents);
        assertTrue(boughtEvents.size() >= 2);

        assertEquals("junit1_item", boughtEvents.get(0).getItem());
        assertEquals("junit2_item", boughtEvents.get(1).getItem());
    }

    @Test
    public void testProcessNoParser() {
        List<String> chunk = new ArrayList<>();
        chunk.add("[00:12:47.067] un-match event 1");
        chunk.add("[00:12:47.067] un-match event 2");
        chunkProcessor.syncProcess(902L, chunk);

        List<MatchEvent> events = matchEventRepository.getAllMatchEventByMatchId(902L);

        assertEquals(0, events.size());

    }


}
