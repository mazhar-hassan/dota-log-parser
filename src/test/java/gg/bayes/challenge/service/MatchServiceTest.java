package gg.bayes.challenge.service;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.repository.MatchEventRepository;
import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroItems;
import gg.bayes.challenge.rest.model.HeroKills;
import gg.bayes.challenge.rest.model.HeroSpells;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@SpringBootTest
public class MatchServiceTest {

    private static Long matchId;
    @Autowired
    MatchService matchService;

    @Autowired
    MatchEventRepository matchEventRepository;

    @BeforeEach
    public void init() throws InterruptedException {
        if (matchId == null) {
            StringBuilder payload = new StringBuilder("[00:12:47.067] npc_dota_hero_dragon_knight buys item item_bracer");
            payload.append("\n");
            payload.append("[00:08:43.460] npc_dota_hero_pangolier casts ability pangolier_swashbuckle (lvl 1) on dota_unknown");
            payload.append("\n");
            payload.append("[00:11:17.489] npc_dota_hero_snapfire is killed by npc_dota_hero_mars");
            payload.append("\n");
            payload.append("[00:36:44.521] npc_dota_hero_death_prophet's death_prophet_spirit_siphon heals npc_dota_hero_death_prophet for 38 health (1919->1957)");
            payload.append("\n");
            payload.append("[00:10:42.031] npc_dota_hero_abyssal_underlord hits npc_dota_hero_bloodseeker with abyssal_underlord_firestorm for 18 damage (720->702)");
            payload.append("\n");
            payload.append("[00:11:17.489] npc_dota_hero_snapfire is killed by npc_dota_hero_mars");
            payload.append("\n");
            payload.append("[00:09:07.521] npc_dota_hero_pangolier uses item_quelling_blade");
            matchId = matchService.ingestMatch(payload.toString());
            assertNotNull(matchId);

            Thread.sleep(5000);
            List<MatchEvent> events = matchEventRepository.getAllMatchEventByMatchId(matchId);
            log.info("Created events on startup: {}", events);

        }
    }

    @Test
    public void testGetAllHeroKills() {
        List<HeroKills> result = matchService.getAllHeroKills(matchId);
        log.info("Result: {}", result);
        Optional<HeroKills> selectedEvent = result.stream()
                .filter(event -> "mars".equals(event.getHero()))
                .filter(event -> 2 == event.getKills())
                .findAny();

        assertTrue(selectedEvent.isPresent());
    }

    @Test
    public void testGetAllPurchaseTimeOfItems() {
        List<HeroItems> result = matchService.getAllPurchaseTimeOfItems(matchId, "dragon_knight");
        log.info("Result: {}", result);
        Optional<HeroItems> selectedEvent = result.stream()
                .filter(event -> "bracer".equals(event.getItem()))
                .filter(event -> Long.valueOf(767067).equals(event.getTimestamp()))
                .findAny();

        assertTrue(selectedEvent.isPresent());
    }

    @Test
    public void testGetAllSpellCountOfHero() {
        List<HeroSpells> result = matchService.getAllSpellCountOfHero(matchId, "pangolier");
        log.info("Result: {}", result);
        Optional<HeroSpells> selectedEvent = result.stream()
                .filter(event -> "swashbuckle".equals(event.getSpell()))
                .filter(event -> Integer.valueOf(1).equals(event.getCasts()))
                .findAny();

        assertTrue(selectedEvent.isPresent());
    }

    @Test
    public void testGetAllDamagedOpponentsStats() {
        List<HeroDamage> result = matchService.getAllDamagedOpponentsStats(matchId, "abyssal_underlord");
        log.info("Result: {}", result);
        Optional<HeroDamage> selectedEvent = result.stream()
                .filter(event -> "bloodseeker".equals(event.getTarget()))
                .filter(event -> Integer.valueOf(1).equals(event.getDamageInstances()))
                .filter(event -> Integer.valueOf(18).equals(event.getTotalDamage()))
                .findAny();

        assertTrue(selectedEvent.isPresent());
    }

    private long ingestMatch(String payload) throws InterruptedException {
        long matchId = matchService.ingestMatch(payload);
        Thread.sleep(1000);

        return matchId;
    }
}
