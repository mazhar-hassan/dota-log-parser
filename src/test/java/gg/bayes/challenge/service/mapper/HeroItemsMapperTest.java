package gg.bayes.challenge.service.mapper;

import gg.bayes.challenge.domain.ValueStats;
import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroItems;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HeroItemsMapperTest {

    @Test
    public void testMap() {
        ValueStats vs = new ValueStats();
        vs.setValue(654321L);
        vs.setKey("junitTarget");
        List<HeroItems> dto = HeroItemsMapper.map(Arrays.asList(vs));

        assertEquals(vs.getValue().intValue(), dto.get(0).getTimestamp());
        assertEquals(vs.getKey(), dto.get(0).getItem());
    }

    @Test
    public void testMapEmpty() {
        List<ValueStats> request = null;
        List<HeroItems> dto = HeroItemsMapper.map(request);

        assertTrue(dto.isEmpty());
    }
}
