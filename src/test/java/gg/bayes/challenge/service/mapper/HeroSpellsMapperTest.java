package gg.bayes.challenge.service.mapper;

import gg.bayes.challenge.domain.ValueStats;
import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroSpells;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HeroSpellsMapperTest {

    @Test
    public void testMap() {
        ValueStats vs = new ValueStats();
        vs.setValue(300L);
        vs.setKey("junitSpell");

        List<HeroSpells> dto = HeroSpellsMapper.map(Arrays.asList(vs));

        assertEquals(vs.getValue().intValue(), dto.get(0).getCasts());
        assertEquals(vs.getKey(), dto.get(0).getSpell());
    }

    @Test
    public void testMapEmpty() {
        List<ValueStats> request = null;
        List<HeroSpells> dto = HeroSpellsMapper.map(request);

        assertTrue(dto.isEmpty());
    }
}
