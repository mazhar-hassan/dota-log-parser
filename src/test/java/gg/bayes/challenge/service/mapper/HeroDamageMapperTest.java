package gg.bayes.challenge.service.mapper;

import gg.bayes.challenge.domain.ValueStats;
import gg.bayes.challenge.rest.model.HeroDamage;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class HeroDamageMapperTest {

    @Test
    public void testMap() {
        ValueStats vs = new ValueStats();
        vs.setValue(5L);
        vs.setSumValue(10L);
        vs.setKey("junitTarget");

        List<HeroDamage> dto = HeroDamageMapper.map(Arrays.asList(vs));

        assertEquals(vs.getValue().intValue(), dto.get(0).getDamageInstances());
        assertEquals(vs.getSumValue().intValue(), dto.get(0).getTotalDamage());
        assertEquals(vs.getKey(), dto.get(0).getTarget());
    }


    @Test
    public void testMapEmpty() {
        List<ValueStats> request = null;
        List<HeroDamage> dto = HeroDamageMapper.map(request);

        assertTrue(dto.isEmpty());
    }
}
