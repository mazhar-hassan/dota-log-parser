package gg.bayes.challenge.service.mapper;

import gg.bayes.challenge.domain.ValueStats;
import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroKills;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HeroKillsMapperTest {

    @Test
    public void testMap() {
        ValueStats vs = new ValueStats();
        vs.setValue(101L);
        vs.setKey("junitTarget");

        List<HeroKills> dto = HeroKillsMapper.map(Arrays.asList(vs));

        assertEquals(vs.getValue().intValue(), dto.get(0).getKills());
        assertEquals(vs.getKey(), dto.get(0).getHero());
    }

    @Test
    public void testMapEmpty() {
        List<ValueStats> request = null;
        List<HeroKills> dto = HeroKillsMapper.map(request);

        assertTrue(dto.isEmpty());
    }
}
