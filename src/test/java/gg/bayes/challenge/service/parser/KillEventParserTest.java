package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KillEventParserTest {
    @Test
    public void testParse() {
        EventParser parser = new KillEventParser();
        Optional<MatchEvent> result = parser.parse("[00:11:17.489] npc_dota_hero_snapfire is killed by npc_dota_hero_mars");
        assertTrue(result.isPresent());

        MatchEvent event = result.get();
        assertEquals(677489, event.getTimestamp());
        assertEquals("mars", event.getActor());
        assertEquals("kill", event.getAction());
        assertEquals("snapfire", event.getOpponent());
    }


    @Test
    public void testParseGoodGuy() {
        EventParser parser = new KillEventParser();
        Optional<MatchEvent> result = parser.parse("[00:37:21.614] npc_dota_goodguys_melee_rax_mid is killed by npc_dota_hero_bloodseeker");
        assertTrue(result.isPresent());

        MatchEvent event = result.get();
        System.out.println(event);
        assertEquals(2241614, event.getTimestamp());
        assertEquals("bloodseeker", event.getActor());
        assertEquals("kill", event.getAction());
        assertEquals("melee_rax_mid", event.getOpponent());
    }
}
