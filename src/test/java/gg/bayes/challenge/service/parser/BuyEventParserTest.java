package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BuyEventParserTest {

    @Test
    public void testParse() {
        BuyEventParser parser = new BuyEventParser();
        Optional<MatchEvent> result = parser.parse("[00:12:47.067] npc_dota_hero_dragon_knight buys item item_bracer");
        assertTrue(result.isPresent());

        MatchEvent event = result.get();
        assertEquals(767067, event.getTimestamp());
        assertEquals("dragon_knight", event.getActor());
        assertEquals("buy", event.getAction());
        assertEquals("bracer", event.getItem());
    }
}
