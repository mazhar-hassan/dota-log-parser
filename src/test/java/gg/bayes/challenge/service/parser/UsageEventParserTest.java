package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UsageEventParserTest {
    @Test
    public void testParse() {
        EventParser parser = new UsageEventParser();
        Optional<MatchEvent> result = parser.parse("[00:09:07.521] npc_dota_hero_pangolier uses item_quelling_blade");
        assertTrue(result.isPresent());

        MatchEvent event = result.get();
        assertEquals(547521, event.getTimestamp());
        assertEquals("pangolier", event.getActor());
        assertEquals("use", event.getAction());
        assertEquals("quelling_blade", event.getItem());
    }
}
