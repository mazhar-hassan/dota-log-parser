package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HitEventParserTest {
    @Test
    public void testParse() {
        EventParser parser = new HitEventParser();
        Optional<MatchEvent> result = parser.parse("[00:13:06.296] npc_dota_hero_mars hits npc_dota_hero_pangolier with mars_spear for 130 damage");
        assertTrue(result.isPresent());

        MatchEvent event = result.get();
        assertEquals(786296, event.getTimestamp());
        assertEquals("mars", event.getActor());
        assertEquals("hit", event.getAction());
        assertEquals("spear", event.getItem());
        assertEquals("pangolier", event.getOpponent());
        assertEquals(130, event.getImpact());
    }

    @Test
    public void testParseExpresion2() {
        EventParser parser = new HitEventParser();
        Optional<MatchEvent> result = parser.parse("[00:10:42.031] npc_dota_hero_abyssal_underlord hits npc_dota_hero_bloodseeker with abyssal_underlord_firestorm for 18 damage (720->702)");
        assertTrue(result.isPresent());

        MatchEvent event = result.get();
        assertEquals(642031, event.getTimestamp());
        assertEquals("abyssal_underlord", event.getActor());
        assertEquals("hit", event.getAction());
        assertEquals("firestorm", event.getItem());
        assertEquals("bloodseeker", event.getOpponent());
        assertEquals(18, event.getImpact());
        assertEquals(720, event.getBeforeImpact());
        assertEquals(702, event.getAfterImpact());

    }
}
