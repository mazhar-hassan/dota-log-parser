package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HealEventParserTest {
    @Test
    public void testParse() {
        EventParser parser = new HealEventParser();
        Optional<MatchEvent> result = parser.parse("[00:37:02.047] npc_dota_hero_bloodseeker's item_vladmir heals npc_dota_hero_bloodseeker for 40 health (1418->1458)");
        assertTrue(result.isPresent());

        MatchEvent event = result.get();
        assertEquals(2222047, event.getTimestamp());
        assertEquals("bloodseeker", event.getActor());
        assertEquals("heal", event.getAction());
        assertEquals("vladmir", event.getItem());
        assertEquals(40, event.getImpact());
    }

    @Test
    public void testParseOtherExpression() {
        EventParser parser = new HealEventParser();
        Optional<MatchEvent> result = parser.parse("[00:36:44.521] npc_dota_hero_death_prophet's death_prophet_spirit_siphon heals npc_dota_hero_death_prophet for 38 health (1919->1957)");
        assertTrue(result.isPresent());

        MatchEvent event = result.get();
        assertEquals(2204521, event.getTimestamp());
        assertEquals("death_prophet", event.getActor());
        assertEquals("heal", event.getAction());
        assertEquals("spirit_siphon", event.getItem());
        assertEquals("death_prophet", event.getOpponent());
        assertEquals(38, event.getImpact());
    }
}
