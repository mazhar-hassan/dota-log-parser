package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CastEventParserTest {
    @Test
    public void testParse() {
        EventParser parser = new CastEventParser();
        Optional<MatchEvent> result = parser.parse("[00:08:43.460] npc_dota_hero_pangolier casts ability pangolier_swashbuckle (lvl 1) on dota_unknown");
        assertTrue(result.isPresent());

        MatchEvent event = result.get();
        assertEquals(523460, event.getTimestamp());
        assertEquals("pangolier", event.getActor());
        assertEquals("cast", event.getAction());
        assertEquals("swashbuckle", event.getItem());
        assertEquals(1, event.getLevel());
    }
}
