package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ChainEventParserTest {

    @Autowired
    ChainEventParser chainEventParser;

    @Test
    public void testParse() {

        Optional<MatchEvent> result = chainEventParser.parse("[00:11:17.489] npc_dota_hero_snapfire is killed by npc_dota_hero_mars");
        assertTrue(result.isPresent());

        MatchEvent event = result.get();
        assertEquals(677489, event.getTimestamp());
        assertEquals("mars", event.getActor());
        assertEquals("kill", event.getAction());
        assertEquals("snapfire", event.getOpponent());
    }

    @Test
    public void testParseFailed() {

        Optional<MatchEvent> result = chainEventParser.parse("[00:08:41.061] game state is now 10");
        assertFalse(result.isPresent());
    }
}
