package gg.bayes.challenge.rest.controller;

import gg.bayes.challenge.common.exception.RecordNotFoundException;
import org.springframework.http.ResponseEntity;

import java.util.Collection;

public interface BusinessController {

    default <T> ResponseEntity<T> respond(T object) {
        if (object == null) {
            throw new RecordNotFoundException("Requested record not found");
        } else if (object instanceof Collection && ((Collection) object).isEmpty()) {
            throw new RecordNotFoundException("Empty result set");
        }

        return ResponseEntity.ok(object);
    }
}
