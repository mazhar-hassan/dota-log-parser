package gg.bayes.challenge;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class ApplicationConfiguration {

    private static final int CORE_POOL_SIZE = 5;
    private static final int MAX_POOL_SIZE = 15;
    private static final int QUEUE_CAPACITY = 200;


    @Bean(name = "logExecutorService")
    public TaskExecutor postReservationExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(CORE_POOL_SIZE);
        executor.setMaxPoolSize(MAX_POOL_SIZE);
        executor.setQueueCapacity(QUEUE_CAPACITY);
        executor.setThreadNamePrefix("PayloadExec-");
        executor.setKeepAliveSeconds(360);
        executor.initialize();

        return executor;
    }
}
