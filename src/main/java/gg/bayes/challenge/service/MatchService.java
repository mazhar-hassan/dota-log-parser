package gg.bayes.challenge.service;

import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroItems;
import gg.bayes.challenge.rest.model.HeroKills;
import gg.bayes.challenge.rest.model.HeroSpells;

import java.util.List;

public interface MatchService {
    Long ingestMatch(String payload);

    List<HeroKills> getAllHeroKills(Long matchId);

    List<HeroItems> getAllPurchaseTimeOfItems(Long matchId, String heroName);

    List<HeroSpells> getAllSpellCountOfHero(Long matchId, String heroName);

    List<HeroDamage> getAllDamagedOpponentsStats(Long matchId, String heroName);
}
