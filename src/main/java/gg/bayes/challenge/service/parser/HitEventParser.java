package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class HitEventParser implements EventParser {
    public final Pattern eventPattern = Pattern.compile("\\[(.*?)\\] npc_dota_hero_(.*?) hits npc_dota_hero_(.*?) with (.*?) for (.*?) damage(.*)");
    public final Pattern impactPattern = Pattern.compile("\\((.*?)->(.*?)\\)");

    @Override
    public Pattern getPattern() {
        return eventPattern;
    }

    @Override
    public void map(MatchEvent event, Matcher matcher) {
        event.setActor(matcher.group(2));
        event.setAction("hit");
        event.setOpponent(matcher.group(3));
        event.setItem(matcher.group(4).replace(event.getActor() + "_", ""));
        event.setImpact(Integer.parseInt(matcher.group(5)));
        if (!StringUtils.isEmpty(matcher.group(6).trim())) {
            populateDamageImpact(event, matcher.group(6).trim());
        }
    }

    private void populateDamageImpact(MatchEvent event, String damageImpact) {
        Matcher subMatcher = impactPattern.matcher(damageImpact);
        if (subMatcher.matches()) {
            event.setBeforeImpact(Integer.parseInt(subMatcher.group(1)));
            event.setAfterImpact(Integer.parseInt(subMatcher.group(2)));
        }
    }
}
