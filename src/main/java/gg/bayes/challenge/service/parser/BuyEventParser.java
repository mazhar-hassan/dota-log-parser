package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class BuyEventParser implements EventParser {

    public static final Pattern eventPattern = Pattern.compile("\\[(.*?)\\] npc_dota_hero_(.*?) buys item item_(.*)");

    @Override
    public Pattern getPattern() {
        return eventPattern;
    }

    @Override
    public void map(MatchEvent event, Matcher matcher) {
        event.setActor(matcher.group(2));
        event.setAction("buy");
        event.setItem(matcher.group(3));
    }
}
