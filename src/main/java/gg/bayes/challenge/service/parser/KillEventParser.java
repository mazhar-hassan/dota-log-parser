package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class KillEventParser implements EventParser {

    private static final Pattern eventPattern = Pattern.compile("\\[(.*?)\\] npc_dota_(hero|goodguys)_(.*) is killed by npc_dota_hero_(.*?)");

    @Override
    public Pattern getPattern() {
        return eventPattern;
    }

    @Override
    public void map(MatchEvent event, Matcher matcher) {
        event.setActor(matcher.group(4));
        event.setAction("kill");
        event.setOpponent(matcher.group(3));
    }
}
