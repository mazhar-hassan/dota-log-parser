package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import gg.bayes.challenge.service.LogParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class ChainEventParser implements LogParser {

    private final List<EventParser> parserChain;

    @Override
    public Optional<MatchEvent> parse(String line) {
        Optional<MatchEvent> event = Optional.empty();

        for (EventParser eventParser : parserChain) {
            event = eventParser.parse(line);
            if (event.isPresent()) {
                break;
            }
        }

        if (!event.isPresent()) {
            log.warn("No parser for line: {}", line);
        }

        return event;
    }

}
