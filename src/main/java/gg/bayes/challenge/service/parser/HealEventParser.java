package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class HealEventParser implements EventParser {


    private static final Pattern eventPattern = Pattern.compile("\\[(.*?)\\] npc_dota_hero_(.*?)'s (.*?) heals npc_dota_hero_(.*?) for (.*?) health \\((.*?)->(.*?)\\)");

    @Override
    public Pattern getPattern() {
        return eventPattern;
    }

    @Override
    public void map(MatchEvent event, Matcher matcher) {
        event.setActor(matcher.group(2));
        event.setAction("heal");
        event.setItem(matcher.group(3).replace("item_", "").replace(event.getActor() + "_", ""));
        event.setOpponent(matcher.group(4));
        event.setImpact(Integer.parseInt(matcher.group(5)));
        event.setBeforeImpact(Integer.parseInt(matcher.group(6)));
        event.setAfterImpact(Integer.parseInt(matcher.group(7)));
    }
}
