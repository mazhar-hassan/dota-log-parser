package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class CastEventParser implements EventParser {
    private static final Pattern eventPattern = Pattern.compile("\\[(.*?)\\] npc_dota_hero_(.*?) casts ability (.*?) \\(lvl (.*?)\\) on (.*)");

    @Override
    public Pattern getPattern() {
        return eventPattern;
    }

    @Override
    public void map(MatchEvent event, Matcher matcher) {
        event.setActor(matcher.group(2));
        event.setAction("cast");
        event.setItem(matcher.group(3).replace(event.getActor() + "_", ""));
        event.setLevel(Integer.parseInt(matcher.group(4)));
        event.setOpponent(matcher.group(5));
    }
}
