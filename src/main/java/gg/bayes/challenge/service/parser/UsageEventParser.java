package gg.bayes.challenge.service.parser;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.service.EventParser;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UsageEventParser implements EventParser {

    public static final Pattern REGEX = Pattern.compile("\\[(.*?)\\] npc_dota_hero_(.*?) uses item_(.*)");

    @Override
    public Pattern getPattern() {
        return REGEX;
    }

    @Override
    public void map(MatchEvent event, Matcher matcher) {
        event.setActor(matcher.group(2));
        event.setAction("use");
        event.setItem(matcher.group(3));
    }
}
