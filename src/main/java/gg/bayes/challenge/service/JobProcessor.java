package gg.bayes.challenge.service;

public interface JobProcessor {
    void process(Long id, String payload);

    void onChunkProcessComplete(Long matchId);
}
