package gg.bayes.challenge.service;

import gg.bayes.challenge.common.utils.TimeUtil;
import gg.bayes.challenge.domain.MatchEvent;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface EventParser extends LogParser {

    Pattern getPattern();

    void map(MatchEvent event, Matcher matcher);

    default MatchEvent create(String timestamp) {
        MatchEvent event = new MatchEvent();
        event.setTimestamp(TimeUtil.toMilliseconds(timestamp));

        return event;
    }

    default Optional<MatchEvent> parse(String line) {
        Matcher result = getPattern().matcher(line);
        MatchEvent event = null;

        if (result.matches()) {
            event = create(result.group(1));
            map(event, result);
        }

        return Optional.ofNullable(event);
    }
}
