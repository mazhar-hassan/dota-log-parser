package gg.bayes.challenge.service;

import gg.bayes.challenge.domain.MatchEvent;

import java.util.Optional;

public interface LogParser {
    Optional<MatchEvent> parse(String line);
}
