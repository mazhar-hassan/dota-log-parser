package gg.bayes.challenge.service;

import java.util.List;

public interface ChunkProcessor {

    void process(Long matchId, List<String> chunk, JobProcessor jobProcessor);

    default void syncProcess(Long matchId, List<String> chunk) {
        process(matchId, chunk, null);
    }
}
