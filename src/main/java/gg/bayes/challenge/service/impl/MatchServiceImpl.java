package gg.bayes.challenge.service.impl;

import gg.bayes.challenge.common.model.DataStatus;
import gg.bayes.challenge.domain.Match;
import gg.bayes.challenge.repository.MatchEventRepository;
import gg.bayes.challenge.repository.MatchRepository;
import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroItems;
import gg.bayes.challenge.rest.model.HeroKills;
import gg.bayes.challenge.rest.model.HeroSpells;
import gg.bayes.challenge.service.JobProcessor;
import gg.bayes.challenge.service.MatchService;
import gg.bayes.challenge.service.mapper.HeroDamageMapper;
import gg.bayes.challenge.service.mapper.HeroItemsMapper;
import gg.bayes.challenge.service.mapper.HeroKillsMapper;
import gg.bayes.challenge.service.mapper.HeroSpellsMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class MatchServiceImpl implements MatchService {

    private final MatchRepository matchRepository;
    private final MatchEventRepository matchEventRepository;
    private final JobProcessor jobProcessor;

    @Override
    public Long ingestMatch(String payload) {

        Match match = new Match();
        match.setStatus(DataStatus.PENDING);
        matchRepository.save(match);

        jobProcessor.process(match.getId(), payload);

        return match.getId();
    }

    @Override
    public List<HeroKills> getAllHeroKills(Long matchId) {
        return HeroKillsMapper.map(matchEventRepository.getAllHeroKills(matchId));
    }

    @Override
    public List<HeroItems> getAllPurchaseTimeOfItems(Long matchId, String heroName) {
        return HeroItemsMapper.map(matchEventRepository.getAllPurchaseTimeOfItems(matchId, heroName));
    }

    @Override
    public List<HeroSpells> getAllSpellCountOfHero(Long matchId, String heroName) {
        return HeroSpellsMapper.map(matchEventRepository.getAllSpellCountOfHero(matchId, heroName));
    }

    @Override
    public List<HeroDamage> getAllDamagedOpponentsStats(Long matchId, String heroName) {
        return HeroDamageMapper.map(matchEventRepository.getAllDamagedOpponentsStats(matchId, heroName));
    }
}
