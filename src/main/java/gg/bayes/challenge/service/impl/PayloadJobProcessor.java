package gg.bayes.challenge.service.impl;

import gg.bayes.challenge.common.model.DataStatus;
import gg.bayes.challenge.domain.Match;
import gg.bayes.challenge.repository.MatchRepository;
import gg.bayes.challenge.service.ChunkProcessor;
import gg.bayes.challenge.service.JobProcessor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service
@RequiredArgsConstructor
public class PayloadJobProcessor implements JobProcessor {
    private final ChunkProcessor chunkProcessor;
    private final MatchRepository matchRepository;
    private static Map<Long, Chunk> currentJobs = new HashMap<>();

    @Async("logExecutorService")
    public void process(Long matchId, String payload) {
        log.info("Processing started for match [{}]", matchId);
        currentJobs.put(matchId, new Chunk());

        int chunkCount = 0;
        try (BufferedReader reader = new BufferedReader(new StringReader(payload))) {
            String line;
            List<String> chunk = new ArrayList<>();
            int rowNumber = 1;
            while ((line = reader.readLine()) != null) {
                chunk.add(line);
                if (rowNumber % 100 == 0) {
                    currentJobs.get(matchId).total = ++chunkCount;
                    chunkProcessor.process(matchId, chunk, this);
                    chunk = new ArrayList<>();
                }
                rowNumber++;
            }

            if (chunk.isEmpty()) {
                return;
            }

            currentJobs.get(matchId).total = ++chunkCount;
            chunkProcessor.process(matchId, chunk, this);
        } catch (IOException e) {
            log.error("Error parsing match payload", e);
        }
    }

    @Override
    public void onChunkProcessComplete(Long matchId) {
        int count = currentJobs.get(matchId).count.incrementAndGet();
        log.info("Processed chunk {} of {}", count, currentJobs.get(matchId).total);
        if (count == currentJobs.get(matchId).total) {
            Optional<Match> match = matchRepository.findById(matchId);
            match.ifPresent(m -> {
                m.setStatus(DataStatus.PROCESSED);
                matchRepository.save(m);
                log.info("All chunks are processed for match Id {}", matchId);
                currentJobs.remove(matchId);
            });
        }
    }

    static class Chunk {
        private int total;
        private AtomicInteger count = new AtomicInteger(0);
    }
}
