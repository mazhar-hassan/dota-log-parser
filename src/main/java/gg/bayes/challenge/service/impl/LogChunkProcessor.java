package gg.bayes.challenge.service.impl;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.repository.MatchEventRepository;
import gg.bayes.challenge.service.ChunkProcessor;
import gg.bayes.challenge.service.JobProcessor;
import gg.bayes.challenge.service.parser.ChainEventParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class LogChunkProcessor implements ChunkProcessor {

    private final ChainEventParser chainEventParser;
    private final MatchEventRepository matchEventRepository;

    @Async
    public void process(Long matchId, List<String> chunk, JobProcessor jobProcessor) {

        for (String line : chunk) {
            Optional<MatchEvent> optionalEvent = chainEventParser.parse(line);
            optionalEvent.ifPresent(event -> {
                event.setMatchId(matchId);
                matchEventRepository.save(event);
            });
        }
        if (jobProcessor != null) {
            jobProcessor.onChunkProcessComplete(matchId);
        }
    }
}
