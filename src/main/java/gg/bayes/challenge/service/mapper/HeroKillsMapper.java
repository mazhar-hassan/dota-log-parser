package gg.bayes.challenge.service.mapper;

import gg.bayes.challenge.domain.ValueStats;
import gg.bayes.challenge.rest.model.HeroKills;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class HeroKillsMapper {

    private HeroKillsMapper() {

    }

    public static List<HeroKills> map(List<ValueStats> heroKills) {
        if (heroKills == null) {
            return Collections.emptyList();
        }

        return heroKills.stream()
                .map(HeroKillsMapper::map)
                .collect(Collectors.toList());
    }

    public static HeroKills map(ValueStats bean) {
        HeroKills dto = new HeroKills();
        dto.setHero(bean.getKey());
        dto.setKills(bean.getValue().intValue());

        return dto;
    }
}
