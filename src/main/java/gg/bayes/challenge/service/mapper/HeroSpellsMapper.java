package gg.bayes.challenge.service.mapper;

import gg.bayes.challenge.domain.ValueStats;
import gg.bayes.challenge.rest.model.HeroSpells;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class HeroSpellsMapper {

    private HeroSpellsMapper() {

    }

    public static HeroSpells map(ValueStats bean) {
        HeroSpells dto = new HeroSpells();
        dto.setSpell(bean.getKey());
        dto.setCasts(bean.getValue().intValue());

        return dto;
    }

    public static List<HeroSpells> map(List<ValueStats> beans) {
        if (beans == null) {
            return Collections.emptyList();
        }

        return beans.stream()
                .map(HeroSpellsMapper::map)
                .collect(Collectors.toList());
    }
}
