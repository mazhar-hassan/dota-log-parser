package gg.bayes.challenge.service.mapper;

import gg.bayes.challenge.domain.ValueStats;
import gg.bayes.challenge.rest.model.HeroDamage;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class HeroDamageMapper {

    private HeroDamageMapper() {

    }

    public static HeroDamage map(ValueStats bean) {
        HeroDamage dto = new HeroDamage();
        dto.setTarget(bean.getKey());
        dto.setDamageInstances(bean.getValue().intValue());
        dto.setTotalDamage(bean.getSumValue().intValue());

        return dto;
    }

    public static List<HeroDamage> map(List<ValueStats> beans) {
        if (beans == null) {
            return Collections.emptyList();
        }

        return beans.stream()
                .map(HeroDamageMapper::map)
                .collect(Collectors.toList());
    }
}
