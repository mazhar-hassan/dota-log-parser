package gg.bayes.challenge.service.mapper;

import gg.bayes.challenge.domain.ValueStats;
import gg.bayes.challenge.rest.model.HeroItems;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class HeroItemsMapper {

    private HeroItemsMapper() {

    }

    public static HeroItems map(ValueStats bean) {
        HeroItems dto = new HeroItems();
        dto.setItem(bean.getKey());
        dto.setTimestamp(bean.getValue());

        return dto;
    }

    public static List<HeroItems> map(List<ValueStats> beans) {
        if (beans == null) {
            return Collections.emptyList();
        }

        return beans.stream()
                .map(HeroItemsMapper::map)
                .collect(Collectors.toList());
    }
}
