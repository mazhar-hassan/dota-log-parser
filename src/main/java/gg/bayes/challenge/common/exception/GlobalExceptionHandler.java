package gg.bayes.challenge.common.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {NoHandlerFoundException.class, HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<ExceptionResponse> handleError404(ServletException exception, HttpServletRequest request) {
        return new ResponseEntity<>(getServletExceptionResponse(exception, request), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ExceptionResponse> handleAllExceptions(Exception exception) {
        String message;
        if (exception instanceof ApplicationException) {
            return handleApplicationException((ApplicationException) exception);
        } else if (exception instanceof NotImplementedException) {
            message = "Feature not supported yes, Please contact site admin";
        } else {
            message = "Unknown error occurred, Please contact site admin";
        }

        log.error("System exception occurred", exception);

        return new ResponseEntity<>(ExceptionResponse.of(ApplicationException.ERROR_UNKNOWN_ERROR, message), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    protected ResponseEntity<ExceptionResponse> handleApplicationException(ApplicationException exception) {
        HttpStatus status;
        if (exception instanceof RecordNotFoundException) {
            status = HttpStatus.NOT_FOUND;
        } else {
            status = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(ExceptionResponse.of(exception), status);
    }

    private ExceptionResponse getServletExceptionResponse(ServletException exception, HttpServletRequest request) {
        int code;
        String message;
        if (exception instanceof NoHandlerFoundException) {
            code = ApplicationException.ERROR_ENDPOINT_NOT_FOUND;
            message = "Endpoint not found: " + request.getRequestURI();

        } else {
            code = ApplicationException.ERROR_HTTP_METHOD_NOT_SUPPORTED;
            message = "HTTP Method not supported: " + request.getMethod();
        }

        return ExceptionResponse.of(code, message);
    }

}
