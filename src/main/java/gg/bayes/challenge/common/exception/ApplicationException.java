package gg.bayes.challenge.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ApplicationException extends RuntimeException {

    public static final int ERROR_UNKNOWN_ERROR = 1000;
    public static final int ERROR_RECORD_NOT_FOUND = 1001;
    public static final int ERROR_REQUIRED_FIELD_MISSING = 1002;
    public static final int ERROR_ENDPOINT_NOT_FOUND = 1003;
    public static final int ERROR_HTTP_METHOD_NOT_SUPPORTED = 1004;


    private final int errorCode;

    public ApplicationException(int errorCode, String message) {
        this(errorCode, message, null);
    }

    public ApplicationException(int errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

}
