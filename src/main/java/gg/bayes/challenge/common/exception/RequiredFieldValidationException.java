package gg.bayes.challenge.common.exception;

public class RequiredFieldValidationException extends ApplicationException {
    public RequiredFieldValidationException(String message) {
        this(message, null);
    }

    public RequiredFieldValidationException(String message, Throwable cause) {
        super(ERROR_REQUIRED_FIELD_MISSING, message, cause);
    }
}
