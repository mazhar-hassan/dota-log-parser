package gg.bayes.challenge.common.exception;

import lombok.Data;

@Data
public class ExceptionResponse {
    private int code;
    private String message;

    public static ExceptionResponse of(ApplicationException exception) {
        return of(exception.getErrorCode(), exception.getMessage());
    }

    public static ExceptionResponse of(int code, String message) {
        ExceptionResponse response = new ExceptionResponse();
        response.setCode(code);
        response.setMessage(message);

        return response;
    }
}
