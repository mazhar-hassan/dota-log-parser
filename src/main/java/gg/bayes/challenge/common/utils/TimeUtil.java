package gg.bayes.challenge.common.utils;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeUtil {

    private static final DateTimeFormatter TIME_PATTERN = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

    private TimeUtil() {

    }

    public static long toMilliseconds(String timestamp) {
        LocalTime time = LocalTime.parse(timestamp, TIME_PATTERN);

        long result = time.getHour() * 3600000L;
        result += time.getMinute() * 60000L;
        result += time.getSecond() * 1000L;
        result += time.getNano() / 1000000L;

        return result;
    }

}
