package gg.bayes.challenge.common.model;

public enum DataStatus {
    PENDING,
    PROCESSED
}
