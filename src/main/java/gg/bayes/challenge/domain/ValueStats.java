package gg.bayes.challenge.domain;

import lombok.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class ValueStats {
    @NonNull
    private String key;

    @NonNull
    private Long value;

    private Long sumValue;
}
