package gg.bayes.challenge.domain;

import gg.bayes.challenge.common.model.DataStatus;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "dota_match")
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "text")
    private String data;

    private DataStatus status;

}
