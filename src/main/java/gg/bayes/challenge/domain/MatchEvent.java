package gg.bayes.challenge.domain;

import lombok.Data;

import javax.persistence.*;

@Table(name = "dota_match_events")
@Entity
@Data
public class MatchEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long matchId;
    private Long timestamp;
    private String actor;
    private String action;
    private String verb;
    private String item;
    private int impact;
    private int level;
    private int beforeImpact;
    private int afterImpact;
    private String opponent;

}
