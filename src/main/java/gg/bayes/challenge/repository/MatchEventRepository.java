package gg.bayes.challenge.repository;

import gg.bayes.challenge.domain.MatchEvent;
import gg.bayes.challenge.domain.ValueStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MatchEventRepository extends JpaRepository<MatchEvent, Long> {

    @Query("SELECT new gg.bayes.challenge.domain.ValueStats(e.actor, count(e.id)) FROM MatchEvent e WHERE e.matchId = :matchId AND e.action='kill' GROUP BY e.actor")
    List<ValueStats> getAllHeroKills(@Param("matchId") Long matchId);

    @Query("SELECT new gg.bayes.challenge.domain.ValueStats(e.item, e.timestamp) FROM MatchEvent e WHERE e.matchId = :matchId AND e.action='buy' AND e.actor=:actor")
    List<ValueStats> getAllPurchaseTimeOfItems(@Param("matchId") Long matchId, @Param("actor") String actor);

    @Query("SELECT new gg.bayes.challenge.domain.ValueStats(e.item, count(e.id)) FROM MatchEvent e WHERE e.matchId = :matchId AND e.action='cast' AND e.actor=:actor GROUP BY e.item")
    List<ValueStats> getAllSpellCountOfHero(@Param("matchId") Long matchId, @Param("actor") String actor);

    @Query("SELECT new gg.bayes.challenge.domain.ValueStats(e.opponent, count(e.impact), sum(e.impact)) FROM MatchEvent e WHERE e.matchId = :matchId AND e.action='hit' AND e.actor=:actor GROUP BY e.opponent")
    List<ValueStats> getAllDamagedOpponentsStats(@Param("matchId") Long matchId, @Param("actor") String actor);

    List<MatchEvent> getAllMatchEventByMatchIdAndActor(Long matchId, String actor);

    List<MatchEvent> getAllMatchEventByMatchId(Long matchId);
}
